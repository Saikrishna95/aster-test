import java.io.IOException;
import java.time.LocalDateTime;

import com.google.gson.JsonObject;
public class HelloWorld {

    public static JsonObject main(JsonObject args) throws IOException {
        //args is the JsonObject variable which will contain the parameters sent by the Aster CRD k8 object
        System.out.println(args);
        System.out.println("Executed time : " + LocalDateTime.now());
        String name = "Saikrishna";
        if (args.has("name"))
            name = args.getAsJsonPrimitive("name").getAsString(); //the way to obtain the value of the parameter in the string format from the key
        JsonObject response = new JsonObject();
        response.addProperty("greeting", "Hello " + name + "!");
        return response;
    }
}